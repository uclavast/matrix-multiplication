# Accelerator Code Review: Matrix Multiplication

## Reviewers
Jie Wang
holawj@gmail.com

## Problems
### 1. Scalability of design due to complex interconnect of *Cbuf*
In current design, results of matrix C will be read in parallel from the local storage inside each PE, which is in contrast to the previous design that results of matrix C
will be passed across PEs to the 1st PE. This may cause timing problems with more complex data paths. The current design has shown a slack of around -3ns with 256 PEs, however,
there are still sufficient resources left for bigger M. This sacrifice is made due to the convenience of double buffering scheduling.

## Suggestions
### 1. Buffers allocated in device memory for double buffering
Current size of each ping-pong buffer is *sizeof(float)\*data_size\*batch_size*, while due to the nature of ping-pong mechanisms, only half of the 
data inside each buffer are accessed during the execution. This auxiliary memory consumption is suboptimal for designs with scarce DDR memory.

### 2. Effective BRAM utilization for Cbuf
Current design uses *uint512* for all the elements from matrix A, B, and C. Calculations show that the BRAM utilization for *Cbuf* is suboptimal.
Consider the 18kb BRAM on Xilinx devices, which can be configured to use 2 read/write ports in parallel, thus offering 36-bit data width at most with 
depth of *1k/2=512*. For *uint512 c_buf[M\*M/16]* with cyclic partition of 16, we have 16 arrays each with depth of *M\*M/16/16=256* and data width of 512. Therefore,
for each array we are using *512/32=16* (32\*512)BRAMs. And the depth utilized for each BRAM is *256/512=50%*.

Actually this factor will decrease when we are handling smaller size matrix multiplication, namely, with the decrease of M. This may cause problems for 
designs with BRAM limitations. (FYI, the effective utilization for *Cbuf* is *M/512* from calculation)

One solution is to decrease the cyclic partition parameter. However, this also brings a new tradeoff since such change will increase the latency of computation
because results of matrix C will take longer time to write to the *Cbuf* due to the reduced the ports of array.

However, this tradeoff should be taken into consideration for designs with smaller size (M).

### 3. Control Signals across PEs
Current design uses 9-bit for global counters, which are passed to each PE one by one. However, the function used from these counters can be replaced by control signals with fewer bits,
which could save the resource utiliztion further. For example, global counter is used to select the correspond *Bbuf* to compute, while such decision can be made in the first PE and passed to 
next PE using a 1-bit control signal instead.

