/*******************************************************************************
Vendor: Xilinx 
Associated Filename: mmult1.c
Purpose: HLS C matrix multiply kernel example
Revision History: July 1, 2013 - initial release
                                                
*******************************************************************************
Copyright (C) 2013 XILINX, Inc.

This file contains confidential and proprietary information of Xilinx, Inc. and 
is protected under U.S. and international copyright and other intellectual 
property laws.

DISCLAIMER
This disclaimer is not a license and does not grant any rights to the materials 
distributed herewith. Except as otherwise provided in a valid license issued to 
you by Xilinx, and to the maximum extent permitted by applicable law: 
(1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND WITH ALL FAULTS, AND XILINX 
HEREBY DISCLAIMS ALL WARRANTIES AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, 
INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT, OR 
FITNESS FOR ANY PARTICULAR PURPOSE; and (2) Xilinx shall not be liable (whether 
in contract or tort, including negligence, or under any other theory of 
liability) for any loss or damage of any kind or nature related to, arising under 
or in connection with these materials, including for any direct, or any indirect, 
special, incidental, or consequential loss or damage (including loss of data, 
profits, goodwill, or any type of loss or damage suffered as a result of any 
action brought by a third party) even if such damage or loss was reasonably 
foreseeable or Xilinx had been advised of the possibility of the same.

CRITICAL APPLICATIONS
Xilinx products are not designed or intended to be fail-safe, or for use in any 
application requiring fail-safe performance, such as life-support or safety 
devices or systems, Class III medical devices, nuclear facilities, applications 
related to the deployment of airbags, or any other applications that could lead 
to death, personal injury, or severe property or environmental damage 
(individually and collectively, "Critical Applications"). Customer assumes the 
sole risk and liability of any use of Xilinx products in Critical Applications, 
subject only to applicable laws and regulations governing limitations on product 
liability. 

THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS FILE AT 
ALL TIMES.

*******************************************************************************/
#include <string.h>

#include <stdio.h>
#include <string.h>
#include <math.h>
#include "mm.h"

//#define bool int
//#define false 0
//#define true 1
#include "ap_int.h"

extern "C" {
   
    union ufloat {
        float f;
        int u;
    };


    typedef ap_uint<128> uint128;

void shift_right_f(float regs[M])
{
#pragma HLS INLINE
    ap_uint<9> i;
    for(i = M-1; i >= 1; i--)
    {
#pragma HLS UNROLL
        regs[i]=regs[i-1];
    }
}
void shift_right_i(ap_uint<16> regs[M])
{
#pragma HLS INLINE
    ap_uint<9> i;
    for(i = M-1; i >= 1; i--)
    {
#pragma HLS UNROLL
        regs[i]=regs[i-1];
    }
}

void shift_left(float regs[M])
{
#pragma HLS INLINE
    ap_uint<9> i;
    for(i = 0; i < M-1; i++)
    {
#pragma HLS UNROLL
        regs[i]=regs[i+1];
    }
}

/*
void shift_left(T regs[M])
{
#pragma HLS INLINE
    int i;
    for(i = 0; i < M-1; i++)
    {
#pragma HLS UNROLL
        regs[i]=regs[i+1];
    }
}
*/
/*
template<class T>
void shift_right(T regs[M])
{
#pragma HLS INLINE
    int i;
    for(i = M-1; i >= 1; i--)
    {
#pragma HLS UNROLL
        regs[i]=regs[i-1];
    }
}

template<class T>
void shift_left(T regs[M])
{
#pragma HLS INLINE
    int i;
    for(i = 0; i < M-1; i++)
    {
#pragma HLS UNROLL
        regs[i]=regs[i+1];
    }
}
*/
void kernel( uint128 a[M][M/4], uint128 b[M][M/4], float local_c[M][M] )
{
    float regs_a[M];
    float regs_b[M];
#pragma HLS ARRAY_PARTITION variable="regs_a" complete dim="1"
#pragma HLS ARRAY_PARTITION variable="regs_b" complete dim="1"
	float local_b1[M];
	float local_b2[M];
#pragma HLS ARRAY_PARTITION variable="local_b1" complete dim="1"
#pragma HLS ARRAY_PARTITION variable="local_b2" complete dim="1"
    bool local_write_mark[M];
    //ap_uint<1> local_write_mark[M];
#pragma HLS ARRAY_PARTITION variable="local_write_mark" complete dim="1"
    bool local_read_mark[M];
    //ap_uint<1> local_read_mark[M];
#pragma HLS ARRAY_PARTITION variable="local_read_mark" complete dim="1"
    ap_uint<8> local_buffer_counter[M];
#pragma HLS ARRAY_PARTITION variable="local_buffer_counter" complete dim="1"
    ap_uint<8> local_read_counter[M];
#pragma HLS ARRAY_PARTITION variable="local_read_counter" complete dim="1"
    ap_uint<16> local_counter[M];
#pragma HLS ARRAY_PARTITION variable="local_counter" complete dim="1"
    ap_uint<16> counter;
    ap_uint<8> a_i = 0;
    ap_uint<8> a_j = 0;
    ap_uint<8> b_i = 0;
    ap_uint<8> b_j = 0;
    ap_uint<8> c_i[M];

    ap_uint<8> pe;
    for(pe=0; pe<M; pe++)
    {
#pragma HLS UNROLL
        local_write_mark[pe]=false;
        local_read_mark[pe]=false;

        local_buffer_counter[pe]=0;
        local_read_counter[pe]=0;
        c_i[pe]=0;
        local_counter[pe]=0;
    }
    ufloat u1;
    ufloat u2;

#pragma HLS ARRAY_PARTITION variable="c_i" complete dim="1"
    for( counter = 0; counter < M2 + 2*M - 1 ; counter++)
    {
#pragma HLS PIPELINE
        shift_right_f(regs_b);
        shift_right_i(local_counter);
        local_counter[0]=counter;
        if( b_i < M )
        {
            //regs_b[0]=b[b_i][b_j];
            uint128 buffer = b[b_i][b_j/4];
            u1.u = buffer(31+ 32*(b_j%4), 32*(b_j%4));
            regs_b[0]=u1.f;
            b_j++;
            if(b_j>=M)
            {
                b_i++;
                b_j=0;
            }
        }
        if( counter >= M )
        {
            shift_right_f(regs_a);
            //regs_a[0]=a[a_i][a_j];
            uint128 buffer = a[a_i][a_j/4];
            u2.u = buffer(31+ 32*(a_j%4), 32*(a_j%4));
            regs_a[0]=u2.f;
            a_i++;
            if(a_i>=M)
            {
                a_j++;
                a_i=0;
            }
        }
        for( pe = 0; pe < M; pe++ )
        {
#pragma HLS UNROLL
            if( local_counter[pe] >= pe )
            {
                if(local_buffer_counter[pe]==0)
                {
                    if(local_write_mark[pe]==false)
                    {
                        local_b1[pe]=regs_b[pe];
                        local_write_mark[pe]=true;
                    }
                    else
                    {
                        local_b2[pe]=regs_b[pe];
                        local_write_mark[pe]=false;
                    }
                }
                local_buffer_counter[pe]++;
                if(local_buffer_counter[pe]>=M) local_buffer_counter[pe]=0;
            }
            if( local_counter[pe] >= M && local_counter[pe] < M2 + M )
            {
#pragma HLS DEPENDENCE variable="local_c" inter false
                if(local_counter[pe] < 2*M)
                {
                    local_c[c_i[pe]][pe] = (local_read_mark[pe]==false?local_b1[pe]:local_b2[pe])*regs_a[pe];
                }
                else
                {
                    local_c[c_i[pe]][pe] +=(local_read_mark[pe]==false?local_b1[pe]:local_b2[pe])*regs_a[pe];
                }
//                local_c[c_i[pe]][pe]+=(local_read_mark[pe]==false?local_b1[pe]:local_b2[pe])*regs_a[pe];
                local_read_counter[pe]++;
                if(local_read_counter[pe]>=M)
                {
                    local_read_counter[pe]=0;
                    local_read_mark[pe]=!local_read_mark[pe];
                }
                c_i[pe]++;
                if(c_i[pe]>=M) c_i[pe]=0;
            }
        }
    }
}
void memory_reshape( float local_c[M][M], uint128 c[M][M/4])
{
    ap_uint<9> i, j;
    //j2=0;
    uint128 buffer;
    ufloat u1;
    for( i = 0; i < M; i++)
    {
        ap_uint<9> jj = 0;
        for( j = 0; j < M; j++)
        {
#pragma HLS PIPELINE
            u1.f = local_c[i][j];
            buffer(31+(jj)*32, (jj)*32) = u1.u;
            jj++;
            if( jj == 4)
            {
                jj = 0;
                c[i][j/4] = buffer;
            }

            
        }
    }
}
//void mm( uint128 a[M][M/4], uint128 b[M][M/4], uint128 c[M][M/4] )
//{
//	float local_c[M][M];
//#pragma HLS ARRAY_PARTITION variable="local_c" complete dim="2"
//    for(i=0; i < M; i++)
//    {
//        for(j=0; j < M; j++)
//        {
//#pragma HLS PIPELINE
//            shift_left(regs);
//            regs[0]=0;
//        }
//        for(j=0; j < M; j++)
//        {
//#pragma HLS UNROLL
//            local_c[i][j]=regs[j];
//        }
//    }
//    kernel(a,b,local_c);
//    memory_reshape(local_c, c);
    /*
    for(i=0; i < M; i++)
    {
        for(j=0; j < M/4; j++)
        {
#pragma HLS UNROLL
            regs[j]=local_c[i][j];
        }
        for(j=0; j < M; j++)
        {
#pragma HLS PIPELINE
            c[i][j]=regs[0];
            shift_left(regs);
        }
    }
    */
//}


void comm_data( 
        int exec1, uint128 *global_a, uint128 *global_b, unsigned int offset1, uint128 data_recv_a[M][M/4], uint128 data_recv_b[M][M/4],
        int exec2, uint128 data_recv_c[M][M/4], uint128 *global_c, unsigned int offset2 
        )
{
#pragma HLS INLINE OFF
    if( exec1)
    {
        memcpy( *data_recv_a, (void *) (global_a + offset1/sizeof(uint128)*sizeof(float)), M*M*sizeof(float));//(1 N^2)
        memcpy( *data_recv_b, (void *) (global_b + offset1/sizeof(uint128)*sizeof(float)), M*M*sizeof(float));//(1 N^2)
    }
    if( exec2)
    {
        memcpy((void *) (global_c + offset2/sizeof(uint128)*sizeof(float)), *data_recv_c, M*M*sizeof(float));//(1 N^2)
    }
}

void compute_1( int exec, uint128 data_a[M][M/4], uint128 data_b[M][M/4], float local_c[M][M])
{
#pragma HLS INLINE OFF
    if(exec)
    {
        //mm(data_a, data_b, data_c);//(4 N^2)
        kernel(data_a, data_b, local_c);
    }
}

void memory_reshape_exec(int exec, float local_c[M][M], uint128 data_c[M][M/4])
{
#pragma HLS INLINE OFF
    if(exec)
    {
        memory_reshape(local_c, data_c);
    }
}

void kernelOO( int i, int n_samples, 
        uint128 *global_a, 
        uint128 *global_b,
        uint128 *global_c,
        uint128 data_recv_a0[M][M/4],
        uint128 data_recv_a1[M][M/4],
        uint128 data_recv_b0[M][M/4],
        uint128 data_recv_b1[M][M/4],
        float   local_c0[M][M],
        float   local_c1[M][M],
        uint128 data_recv_c0[M][M/4],
        uint128 data_recv_c1[M][M/4]
        )
{
#pragma HLS INLINE OFF
    comm_data( 
            i < n_samples, global_a, global_b, i*M2, data_recv_a0, data_recv_b0, 
            i > 2  && i < n_samples + 3, data_recv_c1, global_c, (i-3)*M2
            );
    compute_1( i > 0 && i < n_samples + 1, data_recv_a1, data_recv_b1, local_c0);
    memory_reshape_exec( i > 1 && i < n_samples + 2, local_c1, data_recv_c0);
    
}

void mmult(uint128 *global_a, uint128 *global_b, uint128 *global_c, int n_samples)
{
#pragma HLS INTERFACE m_axi port=global_a offset=slave bundle=gmem
#pragma HLS INTERFACE m_axi port=global_b offset=slave bundle=gmem
#pragma HLS INTERFACE m_axi port=global_c offset=slave bundle=gmem
#pragma HLS INTERFACE s_axilite port=global_a bundle=control
#pragma HLS INTERFACE s_axilite port=global_b bundle=control
#pragma HLS INTERFACE s_axilite port=global_c bundle=control
#pragma HLS INTERFACE s_axilite port=n_samples bundle=control
#pragma HLS INTERFACE s_axilite port=return bundle=control

  const int rank = M;
  int running = 0;
  uint128 bufa0[M][M/4];
  uint128 bufa1[M][M/4];
  uint128 bufb0[M][M/4];
  uint128 bufb1[M][M/4];
  uint128 bufc0[M][M/4];
  uint128 bufc1[M][M/4];
  float   locc0[M][M];
#pragma HLS ARRAY_PARTITION variable="locc0" complete dim="2"
  float   locc1[M][M];
#pragma HLS ARRAY_PARTITION variable="locc1" complete dim="2"
  int i;
  int counter = 0;
  TOP_LOOP: for(i = 0; i < n_samples + 3 ; i++)
  {    
      if( counter == 0 )
      {
        kernelOO( i, n_samples, global_a, global_b, global_c, bufa0, bufa1, bufb0, bufb1, locc0, locc1, bufc0, bufc1);
      }
      else
      {
        kernelOO( i, n_samples, global_a, global_b, global_c, bufa1, bufa0, bufb1, bufb0, locc1, locc0, bufc1, bufc0);
      }
      counter++;
      if( counter >= 2) counter = 0;
  }


  return;
}

}
