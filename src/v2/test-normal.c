#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <assert.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <CL/opencl.h>
#include <sys/time.h>

#define PRINT 0
////////////////////////////////////////////////////////////////////////////////

// Use a static matrix for simplicity
//

////////////////////////////////////////////////////////////////////////////////

int
load_file_to_memory(const char *filename, char **result)
{ 
  int size = 0;
  FILE *f = fopen(filename, "rb");
  if (f == NULL) 
  { 
    *result = NULL;
    return -1; // -1 means file opening fail 
  } 
  fseek(f, 0, SEEK_END);
  size = ftell(f);
  fseek(f, 0, SEEK_SET);
  *result = (char *)malloc(size+1);
  if (size != fread(*result, sizeof(char), size, f)) 
  { 
    free(*result);
    return -2; // -2 means file reading fail 
  } 
  fclose(f);
  (*result)[size] = 0;
  return size;
}

int main(int argc, char** argv)
{
	struct  timeval ts, te, td;
	float tser0;
	float tser1;
	float tser2;
	float tser;

	int batch_size;
	int matrix_size;

	if (argc < 2){
		printf("%s <inputfile>\n", argv[0]);
		return EXIT_FAILURE;
	}
	else {
		char* argv_temp[] = { "exec_name", "bin_name", "1", "256" };
		//char* argv_temp[] = { "exec_name", "bin_name", "1", "4096" };
		//char* argv_temp[] = { "exec_name", "bin_name", "1024", "1024" };
		for( int i = 0 ; i < argc ; i++ )
		{
			argv_temp[i] = argv[i];
		}
		argv = argv_temp;       
	}

	batch_size = atoi(argv[2]);
	printf("batch_size=%d\n", batch_size);
	matrix_size = atoi(argv[3]);
	printf("matrix_size=%d\n", matrix_size);

	int data_size = matrix_size * matrix_size;
	int all_data_size = data_size * batch_size;

	int err;                            // error code returned from api calls

	float *a = (float *)malloc(sizeof(float)*all_data_size);                   // original data set given to device
	float *b = (float *)malloc(sizeof(float)*all_data_size);                   // original data set given to device
	float *results =(float *) malloc(sizeof(float)*all_data_size);             // results returned from device
	float *results2=(float *) malloc(sizeof(float)*all_data_size);             // results returned from device
	//float *results3[all_data_size];             // results returned from device
	float *sw_results=(float *)malloc(sizeof(float)*all_data_size);          // results returned from device

	size_t global[2];                   // global domain size for our calculation
	size_t local[2];                    // local domain size for our calculation

	cl_platform_id platform_id;         // platform id
	cl_device_id device_id;             // compute device id 
	cl_context context;                 // compute context
	cl_command_queue commands;          // compute command queue
	cl_program program;                 // compute program
	cl_kernel kernel;                   // compute kernel

	char cl_platform_vendor[1001];
	char cl_platform_name[1001];

	cl_mem input_a;                     // device memory used for the input array
	cl_mem input_b;                     // device memory used for the input array
	cl_mem output;                      // device memory used for the output array


	// Fill our data sets with pattern
	//
	int i = 0;
	for(i = 0; i < all_data_size; i++) {
		a[i] = (float)i*0.001;
		b[i] = (float)i*0.001;
		results[i] = 0.0;
	}

	// Connect to first platform
	//
	err = clGetPlatformIDs(1,&platform_id,NULL);
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to find an OpenCL platform!\n");
		printf("Test failed\n");
		return EXIT_FAILURE;
	}
	err = clGetPlatformInfo(platform_id,CL_PLATFORM_VENDOR,1000,(void *)cl_platform_vendor,NULL);
	if (err != CL_SUCCESS)
	{
		printf("Error: clGetPlatformInfo(CL_PLATFORM_VENDOR) failed!\n");
		printf("Test failed\n");
		return EXIT_FAILURE;
	}
	printf("CL_PLATFORM_VENDOR %s\n",cl_platform_vendor);
	err = clGetPlatformInfo(platform_id,CL_PLATFORM_NAME,1000,(void *)cl_platform_name,NULL);
	if (err != CL_SUCCESS)
	{
		printf("Error: clGetPlatformInfo(CL_PLATFORM_NAME) failed!\n");
		printf("Test failed\n");
		return EXIT_FAILURE;
	}
	printf("CL_PLATFORM_NAME %s\n",cl_platform_name);

	// Connect to a compute device
	//
	int fpga = 0;
#if defined (FPGA_DEVICE)
	fpga = 1;
#endif
	err = clGetDeviceIDs(platform_id, fpga ? CL_DEVICE_TYPE_ACCELERATOR : CL_DEVICE_TYPE_CPU,
			1, &device_id, NULL);
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to create a device group!\n");
		printf("Test failed\n");
		return EXIT_FAILURE;
	}

	// Create a compute context 
	//
	context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
	if (!context)
	{
		printf("Error: Failed to create a compute context!\n");
		printf("Test failed\n");
		return EXIT_FAILURE;
	}

	// Create a command commands
	//
	commands = clCreateCommandQueue(context, device_id, 0, &err);
	if (!commands)
	{
		printf("Error: Failed to create a command commands!\n");
		printf("Error: code %i\n",err);
		printf("Test failed\n");
		return EXIT_FAILURE;
	}

	int status;

	// Create Program Objects
	//

	// Load binary from disk
	unsigned char *kernelbinary;
	char *xclbin=argv[1];
	printf("loading %s\n", xclbin);
	int n_i = load_file_to_memory(xclbin, (char **) &kernelbinary);
	if (n_i < 0) {
		printf("failed to load kernel from xclbin: %s\n", xclbin);
		printf("Test failed\n");
		return EXIT_FAILURE;
	}
	size_t n = n_i;
	// Create the compute program from offline
	program = clCreateProgramWithBinary(context, 1, &device_id, &n,
			(const unsigned char **) &kernelbinary, &status, &err);
	if ((!program) || (err!=CL_SUCCESS)) {
		printf("Error: Failed to create compute program from binary %d!\n", err);
		printf("Test failed\n");
		return EXIT_FAILURE;
	}

	// Build the program executable
	//
	err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
	if (err != CL_SUCCESS)
	{
		size_t len;
		char buffer[2048];

		printf("Error: Failed to build program executable!\n");
		clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
		printf("%s\n", buffer);
		printf("Test failed\n");
		return EXIT_FAILURE;
	}

	// Create the compute kernel in the program we wish to run
	//
	kernel = clCreateKernel(program, "mmult", &err);
	if (!kernel || err != CL_SUCCESS)
	{
		printf("Error: Failed to create compute kernel!\n");
		printf("Test failed\n");
		return EXIT_FAILURE;
	}

	// Create the input and output arrays in device memory for our calculation
	//
	input_a = clCreateBuffer(context,  CL_MEM_READ_ONLY,  sizeof(float) * all_data_size, NULL, NULL);
	input_b = clCreateBuffer(context,  CL_MEM_READ_ONLY,  sizeof(float) * all_data_size, NULL, NULL);
	output = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(float) * all_data_size, NULL, NULL);
	if (!input_a || !input_b || !output)
	{
		printf("Error: Failed to allocate device memory!\n");
		printf("Test failed\n");
		return EXIT_FAILURE;
	}    


	// Set the arguments to our compute kernel
	//
	err = 0;
	err  = clSetKernelArg(kernel, 0, sizeof(cl_mem), &input_a);
	err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &input_b);
	err |= clSetKernelArg(kernel, 2, sizeof(cl_mem), &output);
	err |= clSetKernelArg(kernel, 3, sizeof(int), &batch_size);
	err |= clSetKernelArg(kernel, 4, sizeof(int), &matrix_size);
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to set kernel arguments! %d\n", err);
		printf("Test failed\n");
		return EXIT_FAILURE;
	}
	cl_event write_event[2];
	cl_event exec_event;
	cl_event read_event;

		//transfer matrix A to device
		err = clEnqueueWriteBuffer(commands, input_a, CL_TRUE, 0, sizeof(float) * all_data_size, a, 0, NULL, &write_event[0]);
		if (err != CL_SUCCESS)
		{
			printf("Error: Failed to write to source array a!\n");
			printf("Test failed\n");
			return EXIT_FAILURE;
		}

		//transfer matrix B to device
		err = clEnqueueWriteBuffer(commands, input_b, CL_TRUE, 0, sizeof(float) * all_data_size, b, 0, NULL, &write_event[1]);
		if (err != CL_SUCCESS)
		{
			printf("Error: Failed to write to source array b!\n");
			printf("Test failed\n");
			return EXIT_FAILURE;
		}
		clWaitForEvents(2, write_event);
		//execute kernel (this is meaningless.... inserted due to bug in AD)
		err = clEnqueueTask(commands, kernel, 0, NULL, NULL);
		err = clEnqueueReadBuffer( commands, output, CL_TRUE, 0, sizeof(float), results, 0, NULL, &read_event ); 
		clWaitForEvents(1, &read_event);


		gettimeofday(&ts, NULL);
		//execute kernel 
		err = clEnqueueTask(commands, kernel, 0, NULL, NULL);
		if (err)
		{
			printf("Error: Failed to execute kernel! %d\n", err);
			printf("Test failed\n");
			return EXIT_FAILURE;
		}
		//transfer matrix C from device (this is meaningless.... inserted due to bug in AD)
		err = clEnqueueReadBuffer( commands, output, CL_TRUE, 0, sizeof(float), results, 0, NULL, &read_event ); 
		clWaitForEvents(1, &read_event);

		gettimeofday(&te, NULL);
		timersub(&ts, &te, &td);
		tser1 = fabs(td.tv_sec+(float)td.tv_usec/1000000.0);
		printf("exec time: %.6fs\n", tser1);
		printf("kernel throughput: %8.4fGFLOPS/s\n", (float)matrix_size*matrix_size*matrix_size*2*batch_size/tser1/1000000000);

		//transfer matrix C from device 
		err = clEnqueueReadBuffer( commands, output, CL_TRUE, 0, sizeof(float) * all_data_size, results, 0, NULL, &read_event ); 
		if(PRINT)
		{ 
			if (err != CL_SUCCESS)
			{
				printf("Error: Failed to read output array! %d\n", err);
				printf("Test failed\n");
				return EXIT_FAILURE;
			}
		}
		clWaitForEvents(1, &read_event);

	//printint matrices (for debugging)
	int ss;
	if(PRINT)
	{ 
		for(ss = 0; ss < 2; ss++)
		{
			printf("A\n");
			for (i=0;i<data_size;i++) {
				printf("%5.2f ",a[i + ss*data_size]);
				if (((i+1) % matrix_size) == 0)
					printf("\n");
			}
			printf("B\n");
			for (i=0;i<data_size;i++) {
				printf("%5.2f ",b[i + ss*data_size]);
				if (((i+1) % matrix_size) == 0)
					printf("\n");
			}
			printf("res\n");
			for (i=0;i<data_size;i++) {
				printf("%5.2f ",results[i + ss*data_size]);
				if (((i+1) % matrix_size) == 0)
					printf("\n");
			}
		}
	}  
	// Validate our results
	//
	//int ss;
	for(ss = 0; ss < batch_size; ss++)
	{ 
		for(i = 0; i < data_size; i++)
		{
			int row = i/matrix_size;
			int col = i%matrix_size;
			float running = 0.0;
			int index;
			for (index=0;index<matrix_size;index++) {
				int aIndex = row*matrix_size + index + ss*data_size;
				int bIndex = col + index*matrix_size + ss*data_size;
				running += a[aIndex] * b[bIndex];
			}
			sw_results[i + ss*data_size] = running;
		}
	}


	int correct = 0;
	int incorrect = 0;
	for (i = 0;i < all_data_size; i++){
		if( fabs(results[i] - sw_results[i]) < 0.0001*fabs(sw_results[i]) ){
			correct++;
		}    
		else{
			printf("(%d) sw:%f hw:%f\n", i, sw_results[i], results[i]);
			incorrect++;
			if( incorrect >= 30 ){
				return EXIT_FAILURE;
			}
		}
	}
	printf("Software\n");

	if(PRINT)
	{ 
		for(ss = 0; ss < 2; ss++)
		{
			for (i=0;i<data_size;i++)
			{
				//printf("%0.2f ",sw_results[i]);
				printf("%5.2f ",sw_results[i + ss*data_size]);
				if (((i+1) % matrix_size) == 0)
					printf("\n");
			}
		}
	} 
	// Print a brief summary detailing the results
	//
	printf("Computed '%d/%d' correct values!\n", correct, all_data_size);

	// Shutdown and cleanup
	//
	clReleaseMemObject(input_a);
	clReleaseMemObject(input_b);
	clReleaseMemObject(output);
	clReleaseProgram(program);
	clReleaseKernel(kernel);
	clReleaseCommandQueue(commands);
	clReleaseContext(context);

	if(correct == all_data_size){
		printf("Test passed!\n");
		return EXIT_SUCCESS;
	}
	else{
		printf("Test failed\n");
		return EXIT_FAILURE;
	}
}
