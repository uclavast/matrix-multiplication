#include <string.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <ap_int.h>

#define M 256
#define M2 (M*M) 

extern "C" {
   
    union ufloat {
        float f;
        int u;
    };

    typedef ap_uint<512> uint512;

void shift_right_f(float regs[M])
{
#pragma HLS INLINE
    ap_uint<9> i;
    for(i = M-1; i >= 1; i--)
    {
#pragma HLS UNROLL
        regs[i]=regs[i-1];
    }
}
void shift_right_i(ap_uint<18> regs[M])
{
#pragma HLS INLINE
    ap_uint<9> i;
    for(i = M-1; i >= 1; i--)
    {
#pragma HLS UNROLL
        regs[i]=regs[i-1];
    }
}

void shift_left(float regs[M])
{
#pragma HLS INLINE
    ap_uint<9> i;
    for(i = 0; i < M-1; i++)
    {
#pragma HLS UNROLL
        regs[i]=regs[i+1];
    }
}

void kernel( int c_init, uint512 a[M][M/16], uint512 b[M][M/16], uint512 c[M*M/16] )
{
	float regs_a[M];
	float regs_b[M];
#pragma HLS ARRAY_PARTITION variable="regs_a" complete dim="1"
#pragma HLS ARRAY_PARTITION variable="regs_b" complete dim="1"
	float local_b1[M];
	float local_b2[M];
#pragma HLS ARRAY_PARTITION variable="local_b1" complete dim="1"
#pragma HLS ARRAY_PARTITION variable="local_b2" complete dim="1"
	ap_uint<18> local_counter[M];
#pragma HLS ARRAY_PARTITION variable="local_counter" complete dim="1"
	float local_c[M*M];
#pragma HLS ARRAY_PARTITION variable="local_c" cyclic factor=256 dim="1"

	for(ap_uint<9> pe=0; pe<M; pe++)
	{
#pragma HLS UNROLL
		local_counter[pe]=0;
		regs_a[pe] = 0;
	}

	for(ap_uint<18> counter = 0; counter < M2 + 2*M ; counter++)
	{
#pragma HLS PIPELINE
		// shifts counter data down the PEs
		shift_right_i(local_counter);
		local_counter[0]=counter;

		// shifts matrix B data down the PEs
		shift_right_f(regs_b);

		ap_uint<9> b_i = counter/M;
		ap_uint<9> b_j = counter%M;

		// extracts single float element from 512b B matrix data
		if( b_i < M )
		{
			//regs_b[0]=b[b_i][b_j];
			uint512 buffer = b[b_i][b_j/16];
			ufloat u1;
			//u1.u = buffer(31+ 32*(b_j%16), 32*(b_j%16));
			switch( b_j%16 ){
				case 0 :
					u1.u = buffer(31+ 32*0, 32*0);
					break;
				case 1 :
					u1.u = buffer(31+ 32*1, 32*1);
					break;
				case 2 :
					u1.u = buffer(31+ 32*2, 32*2);
					break;
				case 3 :
					u1.u = buffer(31+ 32*3, 32*3);
					break;
				case 4 :
					u1.u = buffer(31+ 32*4, 32*4);
					break;
				case 5 :
					u1.u = buffer(31+ 32*5, 32*5);
					break;
				case 6 :
					u1.u = buffer(31+ 32*6, 32*6);
					break;
				case 7 :
					u1.u = buffer(31+ 32*7, 32*7);
					break;
				case 8 :
					u1.u = buffer(31+ 32*8, 32*8);
					break;
				case 9 :
					u1.u = buffer(31+ 32*9, 32*9);
					break;
				case 10 :
					u1.u = buffer(31+ 32*10, 32*10);
					break;
				case 11 :
					u1.u = buffer(31+ 32*11, 32*11);
					break;
				case 12 :
					u1.u = buffer(31+ 32*12, 32*12);
					break;
				case 13 :
					u1.u = buffer(31+ 32*13, 32*13);
					break;
				case 14 :
					u1.u = buffer(31+ 32*14, 32*14);
					break;
				default :
					u1.u = buffer(31+ 32*15, 32*15);
					break;
			}
			regs_b[0]=u1.f;
		}

		// shifts matrix A data down the PEs
		shift_right_f(regs_a);

		// extracts single float element from 512b A matrix data
		if( counter >= M && counter < M2 + M )
		{
			ap_uint<9> a_i = (counter-M)%M;
			ap_uint<9> a_j = (counter-M)/M;

			//shift_right_f(regs_a);
			//regs_a[0]=a[a_i][a_j];
			uint512 buffer = a[a_i][a_j/16];
			ufloat u2;
			//u2.u = buffer(31+ 32*(a_j%16), 32*(a_j%16));
			switch( a_j%16 ){
				case 0 :
					u2.u = buffer(31+ 32*0, 32*0);
					break;
				case 1 :
					u2.u = buffer(31+ 32*1, 32*1);
					break;
				case 2 :
					u2.u = buffer(31+ 32*2, 32*2);
					break;
				case 3 :
					u2.u = buffer(31+ 32*3, 32*3);
					break;
				case 4 :
					u2.u = buffer(31+ 32*4, 32*4);
					break;
				case 5 :
					u2.u = buffer(31+ 32*5, 32*5);
					break;
				case 6 :
					u2.u = buffer(31+ 32*6, 32*6);
					break;
				case 7 :
					u2.u = buffer(31+ 32*7, 32*7);
					break;
				case 8 :
					u2.u = buffer(31+ 32*8, 32*8);
					break;
				case 9 :
					u2.u = buffer(31+ 32*9, 32*9);
					break;
				case 10 :
					u2.u = buffer(31+ 32*10, 32*10);
					break;
				case 11 :
					u2.u = buffer(31+ 32*11, 32*11);
					break;
				case 12 :
					u2.u = buffer(31+ 32*12, 32*12);
					break;
				case 13 :
					u2.u = buffer(31+ 32*13, 32*13);
					break;
				case 14 :
					u2.u = buffer(31+ 32*14, 32*14);
					break;
				default :
					u2.u = buffer(31+ 32*15, 32*15);
					break;
			}
			regs_a[0]=u2.f;
		}
		else{
			regs_a[0]=0;
		}
		for(ap_uint<9> pe = 0; pe < M; pe++ )
		{
#pragma HLS UNROLL
#pragma HLS DEPENDENCE variable="local_b1" inter false
#pragma HLS DEPENDENCE variable="local_b2" inter false
#pragma HLS DEPENDENCE variable="local_c" inter false

			ap_uint<9> c_i = local_counter[pe]%M;
			ap_uint<9> c_i_2 = (local_counter[pe]/M)%2;

			if(c_i==pe)
			{
				if(c_i_2)
				{
					local_b2[pe]=regs_b[pe];
				}
				else
				{
					local_b1[pe]=regs_b[pe];
				}
			}
			
			float local_b;
			if(c_i_2){
				local_b = local_b1[pe];
			}
			else{
				local_b = local_b2[pe];
			}

			float local_c_temp;

			//initialize the c matrix when starting a new block
			if( c_init == 1 && local_counter[pe] < M ){
				local_c_temp = 0;
			}
			else{
				local_c_temp = local_c[c_i*M+pe];
			}

			// C[ij] = A[ik]*B[kj]
			//if(local_counter[pe] >= M){
				local_c_temp += local_b*regs_a[pe];
			//}

			//if( local_counter[pe] < M2 + M )
			//{
				local_c[c_i*M+pe] = local_c_temp;				
			//}
			/*
			if( local_counter[pe] >= M && local_counter[pe] < M2 + M )
			{
#pragma HLS DEPENDENCE variable="local_c" inter false
				float local_c_temp = (((local_counter[pe]/M)%2==1)?local_b1[pe]:local_b2[pe])*regs_a[pe];
				ap_uint<9> c_i = local_counter[pe]%M;

				if(c_init ==1 && local_counter[pe] < 2*M)
				{
					local_c[c_i*M+pe] = local_c_temp;
				}
				else
				{
					local_c[c_i*M+pe] +=local_c_temp;
				}
					//local_c[c_i[pe]][pe]+=(local_read_mark[pe]==false?local_b1[pe]:local_b2[pe])*regs_a[pe];
			}
			*/
		}
	}
	
	// groups 16 floats into 512b for efficient DRAM write
	for( ap_uint<9> c_i = 0 ; c_i < M ; c_i++ )
	{	
#pragma HLS PIPELINE	
		for(ap_uint<9> c_j=0; c_j<M/16; c_j++)
		{
#pragma HLS DEPENDENCE variable="local_c" inter false
#pragma HLS DEPENDENCE variable="c" inter false
			ufloat u0, u1, u2, u3, u4, u5, u6, u7, u8, u9, u10, u11, u12, u13, u14, u15;
			u0.f  = local_c[c_i*M+c_j*16+0 ];
			u1.f  = local_c[c_i*M+c_j*16+1 ];
			u2.f  = local_c[c_i*M+c_j*16+2 ];
			u3.f  = local_c[c_i*M+c_j*16+3 ];
			u4.f  = local_c[c_i*M+c_j*16+4 ];
			u5.f  = local_c[c_i*M+c_j*16+5 ];
			u6.f  = local_c[c_i*M+c_j*16+6 ];
			u7.f  = local_c[c_i*M+c_j*16+7 ];
			u8.f  = local_c[c_i*M+c_j*16+8 ];
			u9.f  = local_c[c_i*M+c_j*16+9 ];
			u10.f = local_c[c_i*M+c_j*16+10];
			u11.f = local_c[c_i*M+c_j*16+11];
			u12.f = local_c[c_i*M+c_j*16+12];
			u13.f = local_c[c_i*M+c_j*16+13];
			u14.f = local_c[c_i*M+c_j*16+14];
			u15.f = local_c[c_i*M+c_j*16+15];
			ap_uint<32> u0_u = u0.u;
			ap_uint<32> u1_u = u1.u;
			ap_uint<32> u2_u = u2.u;
			ap_uint<32> u3_u = u3.u;
			ap_uint<32> u4_u = u4.u;
			ap_uint<32> u5_u = u5.u;
			ap_uint<32> u6_u = u6.u;
			ap_uint<32> u7_u = u7.u;
			ap_uint<32> u8_u = u8.u;
			ap_uint<32> u9_u = u9.u;
			ap_uint<32> u10_u = u10.u;
			ap_uint<32> u11_u = u11.u;
			ap_uint<32> u12_u = u12.u;
			ap_uint<32> u13_u = u13.u;
			ap_uint<32> u14_u = u14.u;
			ap_uint<32> u15_u = u15.u;				
				
			uint512 c_temp = ( u15_u, u14_u, u13_u, u12_u, u11_u, u10_u, u9_u, u8_u, u7_u, u6_u, u5_u, u4_u, u3_u, u2_u, u1_u, u0_u );
			c[c_i*M/16+c_j] = c_temp;
		}		
	}	
}
									
void compute_1(int exec, int c_init, uint512 data_a[M][M/16], uint512 data_b[M][M/16], uint512 data_c[M*M/16])
{
#pragma HLS INLINE OFF
    if(exec)
    {
        kernel(c_init, data_a, data_b, data_c);
    }
}

void mem_readA(  short matrix_size, short bb0, short ii0, short jj0, short kk0, 
        int exec0, uint512 *global_a, uint512 data_recv_a[M][M/16]
)
{
    if(exec0)
    {
        int offset_a = (matrix_size*matrix_size*bb0+matrix_size*ii0+kk0)*sizeof(float)/sizeof(uint512);
        for( int l = 0 ; l < M ; l++ )
        {
            memcpy( data_recv_a[l], (void *) (global_a+offset_a), M*sizeof(float));//(1 N^2)
            offset_a += matrix_size*sizeof(float)/sizeof(uint512);
        }
    }
}

void mem_readB(  short matrix_size, short bb0, short ii0, short jj0, short kk0, 
        int exec0, uint512 *global_b, uint512 data_recv_b[M][M/16]
)
{
    if(exec0)
    {
        int offset_b = (matrix_size*matrix_size*bb0+matrix_size*kk0+jj0)*sizeof(float)/sizeof(uint512);
        for( int l = 0 ; l < M ; l++ )
        {
            memcpy( data_recv_b[l], (void *) (global_b+offset_b), M*sizeof(float));//(1 N^2)
            offset_b += matrix_size*sizeof(float)/sizeof(uint512);
        }
    }
}

void mem_writeC(  short matrix_size, short bb3, short ii3, short jj3, short kk3, 
        int exec3, uint512 *global_c, uint512 data_recv_c[M*M/16]
)
{
    if(exec3)
    {
        int offset_c = (matrix_size*matrix_size*bb3+matrix_size*ii3+jj3)*sizeof(float)/sizeof(uint512);
        if( kk3 == matrix_size - M ){
            for( int l = 0 ; l < M ; l++ )
            {
                memcpy((void *) (global_c+offset_c), &data_recv_c[l*M/16], M*sizeof(float));//(1 N^2)
                offset_c += matrix_size*sizeof(float)/sizeof(uint512);
            }
        }
    }
}

void comm_data( short matrix_size, short bb0, short ii0, short jj0, short kk0, short bb3, short ii3, short jj3, short kk3,
        int exec0, uint512 *global_a, uint512 *global_b, uint512 data_recv_a[M][M/16], uint512 data_recv_b[M][M/16],
        int exec3, uint512 data_recv_c[M*M/16], uint512 *global_c 
        )
{
#pragma HLS INLINE OFF
	mem_readA ( matrix_size, bb0, ii0, jj0, kk0, exec0, global_a, data_recv_a );
	mem_readB ( matrix_size, bb0, ii0, jj0, kk0, exec0, global_b, data_recv_b );
	mem_writeC( matrix_size, bb3, ii3, jj3, kk3, exec3, global_c, data_recv_c );
}

void kernelOO( int i, short matrix_size, short kk1, short bb0, short ii0, short jj0, short kk0, short bb2, short ii2, short jj2, short kk2, int n_samples, 
        uint512 *global_a, 
        uint512 *global_b,
        uint512 *global_c,
        uint512 data_recv_a0[M][M/16],
        uint512 data_recv_a1[M][M/16],
        uint512 data_recv_b0[M][M/16],
        uint512 data_recv_b1[M][M/16],
        uint512   data_c0[M*M/16],
        uint512   data_c1[M*M/16]
        )
{
#pragma HLS INLINE OFF
    comm_data( matrix_size, bb0, ii0, jj0, kk0, bb2, ii2, jj2, kk2, 
            i < n_samples, global_a, global_b, data_recv_a0, data_recv_b0, 
            i > 1  && i < n_samples + 2, data_c1, global_c );
    compute_1( i > 0 && i < n_samples + 1, (kk1==0), data_recv_a1, data_recv_b1, data_c0);
    
}

void mmult(uint512 *global_a, uint512 *global_b, uint512 *global_c, int batch_size, int matrix_size)
{
#pragma HLS INTERFACE m_axi port=global_a offset=slave bundle=gmem1 //depth=4096
#pragma HLS INTERFACE m_axi port=global_b offset=slave bundle=gmem2 //depth=4096
#pragma HLS INTERFACE m_axi port=global_c offset=slave bundle=gmem3 //depth=4096
#pragma HLS INTERFACE s_axilite port=global_a bundle=control
#pragma HLS INTERFACE s_axilite port=global_b bundle=control
#pragma HLS INTERFACE s_axilite port=global_c bundle=control
#pragma HLS INTERFACE s_axilite port=batch_size bundle=control
#pragma HLS INTERFACE s_axilite port=matrix_size bundle=control
#pragma HLS INTERFACE s_axilite port=return bundle=control

  const int rank = M;
  int running = 0;
  uint512 bufa0[M][M/16];
  uint512 bufa1[M][M/16];
  uint512 bufb0[M][M/16];
  uint512 bufb1[M][M/16];
  uint512 bufc0[M*M/16];
  uint512 bufc1[M*M/16];
#pragma HLS ARRAY_PARTITION variable="bufc0" cyclic factor=16 dim=1   //factor=M/16
#pragma HLS ARRAY_PARTITION variable="bufc1" cyclic factor=16 dim=1

  short bb0 = 0;
  short ii0 = 0;
  short jj0 = 0; 
  short kk0 = 0;
  short kk1 = 0;
  short bb2 = 0;
  short ii2 = 0;
  short jj2 = 0; 
  short kk2 = 0;


  int n_samples = batch_size * (matrix_size/M) * (matrix_size/M) * (matrix_size/M);
  TOP_LOOP: for(int i = 0; i < n_samples + 2 ; i++)
  {
    // double buffering 
    if( i%2 == 0 )
    {
      kernelOO( i, matrix_size, kk1, bb0, ii0, jj0, kk0, bb2, ii2, jj2, kk2, n_samples, global_a, global_b, global_c, bufa0, bufa1, bufb0, bufb1, bufc0, bufc1);
    }
    else
    {
      kernelOO( i, matrix_size, kk1, bb0, ii0, jj0, kk0, bb2, ii2, jj2, kk2, n_samples, global_a, global_b, global_c, bufa1, bufa0, bufb1, bufb0, bufc1, bufc0);
    }

    // counters for each dimension of a matrix
    // ii0, jj0, kk0 for matrix read
    // kk1 for compute
    // ii2, jj2, kk2 for matrix write
    kk0 += M;
    if( kk0 == matrix_size ){
      kk0 = 0;
      jj0 += M;
      if( jj0 == matrix_size ){
        jj0 = 0;
        ii0 += M;
        if( ii0 == matrix_size ){
          ii0 = 0;
          bb0++;
        } 
      }
    }       

    if( i >= 1 ){
      kk1 += M;
      if( kk1 == matrix_size ){
        kk1 = 0;
      }
    }

    if( i >= 2 ){
      kk2 += M;
      if( kk2 == matrix_size ){
        kk2 = 0;
        jj2 += M;
        if( jj2 == matrix_size ){
          jj2 = 0;
          ii2 += M;
          if( ii2 == matrix_size ){
            ii2 = 0;
            bb2++;
          }
        }
      }
    }
  }

    // 1GB will reset the counter (for HARP, not necessary for AD)
    if( matrix_size*matrix_size*sizeof(float)*batch_size >= (1<<30) ){
    	bb0 = 0;
    	bb2 = 0;
    }

  return;
}

}
