# Matrix Multiplication

## Developer
### Version 1
Peipei Zhou memoryzpp@ucla.edu

### Version 2
Young-kyu Choi ykchoi@cs.ucla.edu

## Description
### Version 1:
This is the systolic array implementation of matrix multiplication. The systolic array avoids the long broadcast wires and uses local communication to improve the timing and routing.
The block size can be set in the mm.h file. Block C, local_c is reused across A and B. 
The algorithm can be refered in the paper 
J.W.Jang, S.B.Choi and V.K.Prasanna. Energy-and time-efficient matrix multiplication on FPGAs. IEEE Transactions on Very Large Scale Integration (VLSI) Systems, 13.11: 1305-1319, 2005.


### Version 2:

#### 1. Additional features

##### A. Matrix blocking

This version supports multiplication of any large square matrix of 2^n size. This is done by dividing the matrix into sub-matrices, which is also known as blocking.

The block size is set to 256x256. Note that block of matrix C [=AxB] is reused across several blocks of A and B. For example for 4096^3 matrix multiplication, the C sub-matrix will be reused for 16 [=4096/256] times while doing multiplication of 256^3 AxB sub-matrix multiplication.

#### 2. PCIE-related optimization

##### A. PCIE overlapping

The kernel execution and the PCIE transfer is overlapped. (This code can be found at test-overlap.cpp.)

Method is similar to double buffering. There are two copies of cl_mem, and kernel execution is done one set of cl_mem, while PCIE transfer is done on the other set of cl_mem.

Please make sure the OpenCL API is in a non-blocking mode, and waiting for previous event. For example, PCIE read from device should be non-blocking (CL_FALSE), and waiting for kernel execution event (exec_event) :
~~~
clEnqueueReadBuffer( ..., CL_FALSE, ..., &exec_event0, ... );
~~~

Obviously, the kernel execution should wait for PCIE write of A and B, and PCIE read of C should wait for kernel execution. But note that PCIE write should wait for completion of previous kernel execution, NOT previous PCIE read of C. The reason is because next PCIE write can start regardless of whether previous PCIE read has finished or not.

#### 3. LUT-related optimization

##### A. Memory reshape reallocation

The 'memory_reshape' module has been moved to 'kernel' module for simplification.

##### B. Avoiding putting double buffer on unrolled memory

We should try not multiplex (e.g. double buffering) the memory (32b C storage) that has been unrolled, since it will consume many LUTs for multiplexer. Instead, the unrolled memory has been made into a local buffer (trading-off BRAM) and used another less-multiplexed (512b C storage) for double buffering.

##### C. Manually putting HLS dependece to false

Explicitly putting HLS dependence to false where applicable not only helps with timing but also helps reduce the size of circuit. It seems that Vivado HLS cannot automatically find all false dependence.

##### D. Simplifying control for unrolled logic

Try to move the control for the unrolled PEs to a single PE. For example, instead of having:

~~~
(unrolled)
if(local_counter[pe] >= M){
   local_c_temp += local_b*regs_a[pe];
}
~~~

It is better to simplify it to :

~~~
(unrolled)
local_c_temp += local_b*regs_a[pe];
~~~

and instead pass 0 to regs_a down the PEs at the exact right timing. This results in complicated circuitry at PE 0 for regs_a value generation, but results in overall less control circuit.

The side effect of this coding style is that the code becomes very hard to read. 

##### E. Reusing counter

Even if the counter is being used for another purpose, try to merge them into the same counter. This will save the LUT usage. This again results in somewhat hard to read code. For example, some counters have been changed to :
~~~
local_counter[pe]%M or (local_counter[pe]/M)%2
~~~

##### F. Avoid using range operator

Although Vivado HLS does support variable(start bit,end bit) operator, this is inefficiently implemented for unknown reason. Instead, manual multiplexing works better. 

For example, instead of using :
~~~
u1.u = buffer(31+ 32*(b_j%16), 32*(b_j%16));
~~~
Try declaring using switch-case :
~~~
switch( b_j%16 ){
   case 0 :
      u1.u = buffer(31+ 32*0, 32*0);
      break;
   ......
   default :
      u1.u = buffer(31+ 32*15, 32*15);
      break;
}
~~~

## Design Specification
### Version 1:
Input / Output argument: 
global_a, global_b, global_c: A,B,C matrix
n_samples: n consecutive matrix multiplication

### Version 2:

#### Input argument:

   1. total_size : Total number of matrices for multiplication. This number should be a multiple of 'batch_size'
   2. batch_size : Number of matrices that is computed in groups
   3. matrix_size : The size of one dimension of a matrix. 

   Example : "16 4 2048" means "16 matrix multiplication will be performed, and 4 of them will be computed in a batch. Each matrix is 2048x2048 in size."


#### Timing: 

-0.335ns slack (but result is correct)

#### Area:

* LUT : 135466
* FF : 212914
* BRAM18K : 736
* DSP48 : 1296

## Optimization:
### Version 1:
1. bandwidth increase from 32 bits to 128, 256, 512 bits
2. double buffering to overlap the DRAM<->BRAM transfer time and computation time.
3. bit-width optimization, decrease the control signal bit-width to further improve the area.

## Code Navigation

### Version 1:
mmult1.cpp mm.h: the source file of the design
example_alphadata.tcl: the build tcl 
simu.tal: the test file 

### Version 2:
* mmult1.cpp : kernel file
* test-normal.cpp : test file without PCIE overlap
* test-overlap.cpp : test file with PCIE overlap
* example_alphadata.tcl: the build tcl script

## Test Results
### Version 1:
M = 200, Frequency = 167, 
expected performance = 2 * 200 * 167 = 66.8GFLOPS
achieved performance = 62.8GFLOPS

- **Environment Setup**
    -  Xilinx SDAccel 2015.1.3
        
    - AlphaData card

### Version 2:
#### Test Environment
* SW : Xilinx SDAccel 2015.1.5
* HW : Alphadata ADM-PCIE-7V3 board

#### Test Parameters
* Number of PEs : 256
* Frequency : 200MHz
* Matrix size : 4096x4096
* Batch size : 1

#### Test Result
* Achieved Performance : 101.1 GFLOPS
